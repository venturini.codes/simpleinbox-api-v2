<?php

use App\Models\Inbox;

it('can create an inbox from data', function (
    string $name,
    string $email,
    bool $custom_smtp = false,
    string $from = null,
    string $server = null,
    int $port = null,
    bool $tls = false,
    string $username = null,
    string $password = null
) {
    $data = [
        'name' => $name,
        'email' => $email,
        'custom_smtp' => $custom_smtp,
        'from' => $from,
        'server' => $server,
        'port' => $port,
        'tls' => $tls,
        'username' => $username,
        'password' => $password,
    ];

    Inbox::create($data);

    $this->assertDatabaseHas('inboxes', $data);
})->with('inboxes');
