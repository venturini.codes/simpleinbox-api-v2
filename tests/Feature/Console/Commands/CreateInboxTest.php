<?php

use Illuminate\Console\Command;

it('can create an inbox from data using the artisan command', function (
    string $name,
    string $email,
    bool $custom_smtp = false,
    string $from = null,
    string $server = null,
    int $port = null,
    bool $tls = false,
    string $username = null,
    string $password = null
) {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    $data = [
        'name' => $name,
        'email' => $email,
        '--custom-smtp' => $custom_smtp,
        '--from' => $from,
        '--server' => $server,
        '--port' => $port,
        '--tls' => $tls,
        '--username' => $username,
        '--password' => $password,
    ];
    $this->artisan('inbox:create', $data)
        ->expectsOutput('Inbox created successfully')
        ->assertExitCode(Command::SUCCESS);
    $this->assertDatabaseHas('inboxes', [
        'name' => $name,
        'email' => $email,
        'custom_smtp' => $custom_smtp,
        'from' => $from,
        'server' => $server,
        'port' => $port,
        'tls' => $tls,
        'username' => $username,
        'password' => $password,
    ]);
})->with('inboxes');
