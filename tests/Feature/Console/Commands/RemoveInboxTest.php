<?php

use App\Models\Inbox;

it('can remove inbox by given id', function () {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    $inbox = Inbox::first();

    $this->artisan('inbox:remove', ['id' => $inbox->id])
        ->expectsOutput('Inbox removed successfully.')
        ->assertSuccessful();

    $this->assertDatabaseHas('inboxes', ['id' => $inbox->id, 'deleted_at' => now()]);
});
