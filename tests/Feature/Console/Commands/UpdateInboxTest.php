<?php

use App\Models\Inbox;

it('can update the name of an inbox by id using an artisan command', function () {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    $inbox = Inbox::first();

    $this->artisan('inbox:update', [
        'id' => $inbox->id,
        '--name' => 'Updated Inbox Name',
    ])->expectsOutput('Inbox updated successfully.');

    $this->assertDatabaseHas('inboxes', [
        'id' => $inbox->id,
        'name' => 'Updated Inbox Name',
    ]);
});

it('can update the email of an inbox by id using an artisan command', function () {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    $inbox = Inbox::first();

    $newEmail = 'thomas@venturini.codes';

    $this->artisan('inbox:update', [
        'id' => $inbox->id,
        '--email' => $newEmail,
    ])->expectsOutput('Inbox updated successfully.');

    $this->assertDatabaseHas('inboxes', [
        'id' => $inbox->id,
        'email' => $newEmail,
    ]);
});

it('can set custom smtp of an inbox to true by id using an artisan command', function () {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    $inbox = Inbox::first();

    $this->artisan('inbox:update', [
        'id' => $inbox->id,
        '--custom-smtp' => true,
        '--from' => $from = 'thomas@venturini.codes',
        '--server' => $server = 'example.com',
        '--username' => $username = 'username',
        '--password' => $password = 'password',
        '--port' => $port = '1234',
        '--tls' => true
    ])
        ->expectsOutput('Inbox updated successfully.')
        ->assertSuccessful();

    $this->assertDatabaseHas('inboxes', [
        'id' => $inbox->id,
        'custom_smtp' => true,
        'from' => $from,
        'server' => $server,
        'username' => $username,
        'password' => $password,
        'port' => $port,
        'tls' => true
    ]);
});
