<?php

use App\Models\Inbox;
use Illuminate\Console\Command;

it('can list all available inboxes using the artisan command', function () {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    $inboxes = Inbox::all()->map(function ($inbox) {
        return [
            $inbox->id,
            $inbox->name,
            $inbox->email,
            $inbox->custom_smtp ? 'Yes' : 'No',
            $inbox->from,
            $inbox->server,
            $inbox->port,
            $inbox->tls ? 'Yes' : 'No',
            $inbox->username,
            $inbox->created_at->format('Y-m-d H:i:s'),
            optional($inbox->deleted_at)->format('Y-m-d H:i:s'),
        ];
    })->toArray();
    $this->artisan('inbox:list')
        ->expectsTable([
            'ID',
            'Name',
            'Email',
            'Custom SMTP',
            'From',
            'Server',
            'Port',
            'TLS',
            'Username',
            'Created At',
            'Deleted At'
        ], $inboxes)
        ->assertExitCode(Command::SUCCESS);
});
