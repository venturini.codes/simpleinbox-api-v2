<?php

use App\Models\Inbox;
use App\Notifications\InboxMessage;
use Illuminate\Support\Facades\Notification;

it('can trigger test email via artisan command', function () {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    Notification::fake();

    $inbox = Inbox::first();

    $this->artisan('inbox:trigger', ['id' => $inbox->id, 'from' => 'thomas@venturini.codes'])
        ->expectsOutput('Successfully trigger test email to inbox.')
        ->assertSuccessful();

    Notification::assertCount(1);
    Notification::assertSentTo([$inbox], InboxMessage::class);
});
