<?php

use App\Models\Inbox;
use Illuminate\Console\Command;

it('can show details of a given inbox by id', function () {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    $inbox = Inbox::factory()->create();

    $values = [
        ['ID', $inbox->id],
        ['Name', $inbox->name],
        ['Email', $inbox->email],
        ['Custom SMTP', $inbox->custom_smtp ? 'Yes' : 'No'],
        ['From', $inbox->from],
        ['Server', $inbox->server],
        ['Port', $inbox->port],
        ['TLS', $inbox->tls ? 'Yes' : 'No'],
        ['Username', $inbox->username],
        ['Created At', $inbox->created_at->format('Y-m-d H:i:s')],
        ['Updated At', $inbox->updated_at->format('Y-m-d H:i:s')],
        ['Deleted At', optional($inbox->deleted_at)->format('Y-m-d H:i:s')],
    ];

    $this->artisan('inbox:show', ['id' => $inbox->id])
        ->expectsTable([
            'Field',
            'Value',
        ], $values)
        ->assertExitCode(Command::SUCCESS);
});
