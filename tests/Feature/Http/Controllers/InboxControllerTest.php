<?php

use App\Models\Inbox;
use App\Notifications\InboxMessage;
use Illuminate\Support\Facades\Notification;

it('can trigger the inbox via the api', function () {
    /** @var Illuminate\Foundation\Testing\TestCase $this  */

    Notification::fake();

    $inbox = Inbox::factory()->create();
    $from = 'thomas@venturini.codes';
    $subject = 'Test Email';
    $message = 'Test message ...';

    $response = $this->postJson(route('inbox.trigger', $inbox->id, false), [
        'from' => $from,
        'subject' => $subject,
        'message' => $message,
    ]);

    $response->assertSuccessful();

    Notification::assertCount(1);
    Notification::assertSentTo([$inbox], InboxMessage::class);
});
