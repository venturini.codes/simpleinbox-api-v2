<?php

dataset('inboxes', [
    'Demo Inbox Minimal' => [
        'name' => 'Demo Inbox Minimal',
        'email' => 'thomas@venturini.codes',
    ],
    'Demo Inbox Custom SMTP' => [
        'name' => 'Demo Inbox Custom SMTP',
        'email' => 'thomas@venturini.codes',
        'custom_smtp' => true,
        'from' => 'thomas@venturini.codes',
        'server' => 'smtp.example.com',
        'port' => 587,
        'tls' => true,
        'username' => 'default',
        'password' => 'password',
    ],
]);
