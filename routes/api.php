<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InboxController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('inbox/{id}/trigger', [InboxController::class, 'trigger'])->name('inbox.trigger');
Route::apiResource('inbox', InboxController::class);
