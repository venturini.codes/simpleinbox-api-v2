help:
	@echo "Usage: make [command]"
	@echo ""
	@echo "Commands:"
	@echo "  init-local    Init project"
	@echo "  init-prod     Init project"
	@echo "  up            Start containers"
	@echo "  logs          Show logs"
	@echo "  tail          Show logs and follow"
	@echo "  start         Start containers and show logs"
	@echo "  down          Stop containers"
	@echo "  stop          Stop containers"
	@echo "  restart       Restart containers and show logs"
	@echo "  login         Login to workspace container"
	@echo "  ps            Show containers status"
	@echo "  build         Build containers"
	@echo "  test          Run tests"

init-local:
	@cp .env.example .env
	@ln -s docker-compose.local.yml docker-compose.yml
	@docker compose --env-file .env.laradock up -d
	@docker compose --env-file .env.laradock exec --user laradock workspace composer install

init-prod:
	@cp .env.example .env
	@ln -s docker-compose.prod.yml docker-compose.yml
	@docker compose --env-file .env.laradock up -d
	@docker compose --env-file .env.laradock exec --user laradock workspace composer install

up:
	@docker compose --env-file .env.laradock up -d

logs:
	@docker compose --env-file .env.laradock logs

tail:
	@docker compose --env-file .env.laradock logs -f

start: up tail

down:
	@docker compose --env-file .env.laradock down --remove-orphans

stop: down

restart: down up tail

login:
	@docker compose --env-file .env.laradock exec --user laradock workspace bash

ps:
	@docker compose --env-file .env.laradock ps

build:
	@docker compose --env-file .env.laradock build

test:
	@docker compose --env-file .env.laradock exec --user laradock workspace php artisan test

login-mysql:
	@docker compose --env-file .env.laradock exec mysql mysql -u default -psecret default
