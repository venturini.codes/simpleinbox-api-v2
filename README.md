# Simple Inbox API v2

A simple api to send emails to a specific inbox via SMTP.

## Setup

> Make sure to have [traefik](https://github.com/tjventurini/traefik-reverse-proxy) running.

```bash
git clone git@gitlab.com/venturini.codes/simple-inbox-api-v2.git
cd simple-inbox-api-v2
make init
```

## URLs

* [api.simpleinbox.localhost](http://api.simpleinbox.localhost)
* [mailhog.api.simpleinbox.localhost](http://mailhog.api.simpleinbox.localhost)
