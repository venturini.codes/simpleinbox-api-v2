<?php

namespace App\Console\Commands;

use App\Models\Inbox;
use Illuminate\Console\Command;

class ShowInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:show {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show details of a given inbox by id.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $inbox = Inbox::findOrFail($this->argument('id'));

        $values = [
            ['ID', $inbox->id],
            ['Name', $inbox->name],
            ['Email', $inbox->email],
            ['Custom SMTP', $inbox->custom_smtp ? 'Yes' : 'No'],
            ['From', $inbox->from],
            ['Server', $inbox->server],
            ['Port', $inbox->port],
            ['TLS', $inbox->tls ? 'Yes' : 'No'],
            ['Username', $inbox->username],
            ['Created At', $inbox->created_at->format('Y-m-d H:i:s')],
            ['Updated At', $inbox->updated_at->format('Y-m-d H:i:s')],
            ['Deleted At', optional($inbox->deleted_at)->format('Y-m-d H:i:s')],
        ];

        $this->table([
            'Field',
            'Value',
        ], $values);
    }
}
