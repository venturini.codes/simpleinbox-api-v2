<?php

namespace App\Console\Commands;

use App\Models\Inbox;
use Illuminate\Console\Command;

class RemoveInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:remove {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove an inbox by id.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $id = $this->argument('id');

        $inbox = Inbox::findOrFail($id);

        $inbox->delete();

        $this->info('Inbox removed successfully.');
    }
}
