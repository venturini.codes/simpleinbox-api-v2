<?php

namespace App\Console\Commands;

use App\Models\Inbox;
use Illuminate\Console\Command;

class ListInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:list {--trashed : Include trashed inboxes}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List the inboxes available in the application.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $query = Inbox::query();

        if ($this->option('trashed')) {
            $query->withTrashed();
        }

        $inboxes = $query->get();

        if ($inboxes->isEmpty()) {
            $this->error('No inboxes found.');

            return Command::FAILURE;
        }

        $headers = [
            'ID',
            'Name',
            'Email',
            'Custom SMTP',
            'From',
            'Server',
            'Port',
            'TLS',
            'Username',
            'Created At',
            'Deleted At'
        ];

        $this->table($headers, $inboxes->map(function (Inbox $inbox) {
            return [
                $inbox->id,
                $inbox->name,
                $inbox->email,
                $inbox->custom_smtp ? 'Yes' : 'No',
                $inbox->from,
                $inbox->server,
                $inbox->port,
                $inbox->tls ? 'Yes' : 'No',
                $inbox->username,
                $inbox->created_at->format('Y-m-d H:i:s'),
                optional($inbox->deleted_at)->format('Y-m-d H:i:s'),
            ];
        }));
    }
}
