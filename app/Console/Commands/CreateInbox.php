<?php

namespace App\Console\Commands;

use Validator;
use App\Models\Inbox;
use Illuminate\Console\Command;

class CreateInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:create
        {name : The name of the inbox.}
        {email : The email of the inbox.}
        {--custom-smtp : Does this inbox use custom SMTP credentials?}
        {--tls : Does the SMTP server use TLS encryption.}
        {--from= : The from address to use.}
        {--server= : The SMTP server to use.}
        {--username= : The SMTP username.}
        {--password= : The SMTP password.}
        {--port= : The port to use.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an inbox.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = [
            'name' => $this->argument('name'),
            'email' => $this->argument('email'),
            'custom_smtp' => $this->option('custom-smtp'),
            'from' => $this->option('from'),
            'server' => $this->option('server'),
            'username' => $this->option('username'),
            'password' => $this->option('password'),
            'port' => $this->option('port'),
            'tls' => $this->option('tls'),
        ];

        $Validator = Validator::make($data, config('validation.inbox.create'));

        if ($Validator->fails()) {
            $this->error('The inbox could not be created.');
            foreach ($Validator->errors()->all() as $error) {
                $this->error($error);
            }
            return Command::FAILURE;
        }

        Inbox::create($Validator->validated());

        $this->info('Inbox created successfully');
        return Command::SUCCESS;
    }
}
