<?php

namespace App\Console\Commands;

use Validator;
use App\Models\Inbox;
use Illuminate\Console\Command;

class TriggerInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:trigger
        {id : The ID of the inbox to trigger the test for.}
        {from : The from address.}
        {--subject=Test Email : The subject.}
        {--message=Test message ... : The message.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a test email to the given inbox.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = array_merge($this->arguments(), $this->options());

        $validator = Validator::make($data, config('validation.inbox.send'));

        $validated = collect($validator->validated());

        Inbox::findOrFail($validated->get('id'))
            ->trigger(...$validated->except('id')->toArray());

        $this->info('Successfully trigger test email to inbox.');
    }
}
