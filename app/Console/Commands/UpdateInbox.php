<?php

namespace App\Console\Commands;

use App\Models\Inbox;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class UpdateInbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:update {id}
    {--name= : The name of the inbox.}
    {--email= : The email of the inbox.}
    {--custom-smtp : Does this inbox use custom SMTP credentials?}
    {--tls : Does the SMTP server use TLS encryption.}
    {--from= : The from address to use.}
    {--server= : The SMTP server to use.}
    {--username= : The SMTP username.}
    {--password= : The SMTP password.}
    {--port= : The port to use.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update an inbox by id';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = [
            'id' => $this->argument('id'),
            'name' => $this->option('name'),
            'email' => $this->option('email'),
            'custom_smtp' => $this->option('custom-smtp'),
            'from' => $this->option('from'),
            'server' => $this->option('server'),
            'username' => $this->option('username'),
            'password' => $this->option('password'),
            'port' => $this->option('port'),
            'tls' => $this->option('tls'),
        ];
        $Validator = Validator::make($data, config('validation.inbox.update'));

        $validated = collect($Validator->validated())
            ->filter();

        Inbox::findOrFail($validated->get('id'))
            ->update($validated->except('id')->toArray());

        $this->info('Inbox updated successfully.');
    }
}
