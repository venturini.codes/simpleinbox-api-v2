<?php

namespace App\Models;

use App\Notifications\InboxMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Inbox extends Model
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;
    use Notifiable;

    /**
     * The database table to use.
     *
     * @var string
     */
    protected $table = 'inboxes';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'custom_smtp' => false,
        'tls' => false,
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'custom_smtp' => 'boolean',
        'tls' => 'boolean',
    ];

    public function trigger(string $from, string $subject, string $message): void
    {
        if ($this->custom_smtp) {
            config([
                'mail.mailers.smtp' => [
                    'transport' => 'smtp',
                    'host' => $this->server,
                    'port' => $this->port,
                    'encryption' => $this->tls === true ? 'tls' : 'ssl',
                    'username' => $this->username,
                    'password' => $this->password,
                ]
            ]);
        }
        $this->notify(new InboxMessage($from, $subject, $message));
    }
}
