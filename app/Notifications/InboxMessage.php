<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class InboxMessage extends Notification
{
    use Queueable;

    /**
     * The from address.
     *
     * @var string
     */
    public string $from;

    /**
     * The subject.
     *
     * @var string
     */
    public string $subject;

    /**
     * The message.
     *
     * @var string
     */
    public string $message;

    /**
     * Create a new notification instance.
     */
    public function __construct(string $from, string $subject, string $message)
    {
        $this->from = $from;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject($this->subject)
            ->replyTo($this->from)
            ->markdown('mail.inbox.message', [
                'from' => $this->from,
                'subject' => $this->subject,
                'message' => $this->message,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'from' => $this->from,
            'subject' => $this->subject,
            'message' => $this->message,
        ];
    }
}
