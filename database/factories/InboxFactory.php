<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Inbox>
 */
class InboxFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
        ];
    }

    public function customSmtp(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'custom_smtp' => true,
                'from' => fake()->unique()->safeEmail(),
                'server' => fake()->domainName(),
                'port' => fake()->numberBetween(1, 9999),
                'tls' => fake()->boolean(),
                'username' => fake()->userName(),
                'password' => fake()->password(),
            ];
        });
    }
}
