<?php

return [
    'inbox' => [
        'create' => [
            'name' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'email:dns',
            ],
            'custom_smtp' => 'boolean',
            'from' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'email:dns',
            ],
            'server' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'username' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'password' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'port' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'integer'
            ],
            'tls' => [
                'exclude_if:custom_smtp,false',
                'sometimes',
                'boolean'
            ]
        ],
        'update' => [
            'id' => [
                'required',
                'exists:inboxes,id'
            ],
            'name' => [
                'nullable',
                'string',
            ],
            'email' => [
                'nullable',
                'email:dns',
            ],
            'custom_smtp' => 'boolean',
            'from' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'email:dns',
            ],
            'server' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'username' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'password' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'string'
            ],
            'port' => [
                'required_if:custom_smtp,true',
                'exclude_if:custom_smtp,false',
                'integer'
            ],
            'tls' => [
                'exclude_if:custom_smtp,false',
                'sometimes',
                'boolean'
            ]
        ],
        'send' => [
            'id' => [
                'required',
                'exists:inboxes,id',
            ],
            'from' => [
                'required',
                'email:dns'
            ],
            'subject' => [
                'required',
                'string',
            ],
            'message' => [
                'required',
                'string',
            ]
        ]
    ]
];
